using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SCM.StudentCourses.Data;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using SCM.StudentCourses.Mapper;
using SCM.StudentCourses.Repositories.Interfaces;
using SCM.StudentCourses.Repositories.Implementations;
using SCM.StudentCourses.Services.Interfaces;
using SCM.StudentCourses.Services.Implementations;
using SCM.StudentCourses.Services.ExternalResources;
using HotChocolate;
using SCM.StudentCourses.GraphQL;
using HotChocolate.Execution.Configuration;
using Microsoft.AspNetCore.Http;
using HotChocolate.AspNetCore;
using System;
namespace SCM.StudentCourses
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var serverName = Configuration["DBServer"] ?? "localhost";
            var port = Configuration["DBPort"] ?? "30005";
            var userName = Configuration["DBUser"] ?? "sa";
            var userPassword = Configuration["DBPassword"] ?? "Password@321";
            var database = Configuration["StudentCourseDb"] ?? "StudentCourseDb";
            var connectionString = Environment
            .GetEnvironmentVariable("MYSQLSERVER_CONNECTIONSTRING");
            services.AddDbContext<StudentCoursesDbContext>(
            o => o.UseMySql(connectionString,ServerVersion.AutoDetect(connectionString)));

            //Configuration.GetConnectionString("StudentCourseDbCS")
            services.AddControllers();
            //services.AddDbContext<StudentCoursesDbContext>(options =>
            //options.UseInMemoryDatabase(Configuration.GetConnectionString("StudentCourseDbCS")),ServiceLifetime.Transient);
            // services.AddDbContext<StudentCoursesDbContext>(options =>
            // options.UseSqlServer($"Server={serverName},{port};Initial Catalog={database};User ID={userName};Password={userPassword}"));
            services.AddScoped<DbContext, StudentCoursesDbContext>();
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new StudentCourseMapper());
                mc.AddProfile(new CourseMapper());
            });
           // Console.Out.WriteLine("from configure services "+connectionString);
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            services.AddTransient<IStudentCourseRepository, StudentCourseRepository>();
            services.AddTransient<IStudentService,StudentService>();  
            services.AddTransient<IExternalUsersService,ExternalUsersService>();  
            services.AddTransient<IStudentCourseService, StudentCourseService>();  
            services.AddTransient<ICourseRepository, CourseRepository>();
            services.AddTransient<ICourseService, CourseService>();

            services.AddGraphQL(sp => SchemaBuilder.New()
            .AddQueryType<QueryType>()
            .AddType<StudentCoursesType>()
            .Create(),
                new QueryExecutionOptions { ForceSerialExecution = true });

            // Register the Swagger services
            services.AddSwaggerDocument();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            DataSeeding.Initializer(app);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            // Register the Swagger generator and the Swagger UI middlewares
            app.UseOpenApi();
            app.UseSwaggerUi3();
            //app.UseHttpsRedirection();
            app.UseCors(x => x
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader()); 
            //app.UseHttpsRedirection();
            app.UseRouting();

            app.UseGraphQL();
            app.UsePlayground();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                //endpoints.MapGet("/");
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync(System.IO.Directory.GetCurrentDirectory());
                });
            });
        }
        private static void UpdateDatabase(IApplicationBuilder app)
        {
            Console.Out.WriteLine("from update database ");
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<StudentCoursesDbContext>())
                {
                    Console.Out.WriteLine("before migrate");
                    context.Database.Migrate();
                     Console.Out.WriteLine("after migrate");
                }
            }
        }
    }
}
