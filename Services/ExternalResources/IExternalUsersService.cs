﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SCM.StudentCourses.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SCM.StudentCourses.Services.ExternalResources
{
    public interface IExternalUsersService
    {
        Task<List<UserDTO>> GetAllUsersAsync();
    }
    public class ExternalUsersService : IExternalUsersService
    {
        private readonly IConfiguration _configuration;
        public ExternalUsersService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<List<UserDTO>> GetAllUsersAsync()
        {
            try
            {
                List<UserDTO> _users;
                using var client = new HttpClient();
                var httpResponse = await client.GetAsync(_configuration["ExternalResources:JsonPlaceholder_BaseUrl"].ToString());
                using (HttpContent content = httpResponse.Content)
                {
                    string json = await content.ReadAsStringAsync();
                    _users = JsonConvert.DeserializeObject<List<UserDTO>>(json);
                }
                return _users;
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}
