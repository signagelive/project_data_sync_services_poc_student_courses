#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app
EXPOSE 80
EXPOSE 443
#copy csproj and restore
COPY *.csproj ./
RUN dotnet restore
#copy everything else and build
COPY . ./
RUN dotnet publish -c Release -o out
#generate run time image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-bionic
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "SCM.StudentCourses.dll"]