﻿using HotChocolate.Types;
using SCM.StudentCourses.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SCM.StudentCourses.GraphQL
{
    public class StudentCoursesType : ObjectType<StudentCoursesGQL>
    {
        protected override void Configure(IObjectTypeDescriptor<StudentCoursesGQL> descriptor)
        {
            descriptor.Field(d => d.CourseId).Type<IdType>();
            descriptor.Field(d => d.CourseName).Type<StringType>();
            descriptor.Field(d => d.CourseNo).Type<StringType>();
            descriptor.Field(d => d.ObtainedCreditHours).Type<FloatType>();
            descriptor.Field(d => d.StudentId).Type<IdType>();
            descriptor.Field(d => d.IsDeleted).Type<BooleanType>();
        }
    }
}
