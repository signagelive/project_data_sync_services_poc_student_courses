﻿using HotChocolate;
using SCM.StudentCourses.DTO;
using SCM.StudentCourses.Services.ExternalResources;
using SCM.StudentCourses.Services.Interfaces;
using SCM.StudentCourses.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SCM.StudentCourses.GraphQL
{
    public class Query
    {
        public async Task<IEnumerable<StudentCoursesGQL>> Courses([Service] IStudentCourseService studentService) => 
            await studentService.GetDetailGQLAsync();

        public async Task<StudentCoursesGQL> GetCoursebyId([Service] IStudentCourseService studentService, string Id) => 
            await studentService.GetCourseById(Id);
        public async Task<IEnumerable<UserDTO>> GetExternalUsers([Service] IExternalUsersService externalUsersService) => 
            await externalUsersService.GetAllUsersAsync();

    }
}
